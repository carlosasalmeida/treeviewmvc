namespace TreeViewMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class renameTable : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Vehicle", newName: "VehicleType");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.VehicleType", newName: "Vehicle");
        }
    }
}
