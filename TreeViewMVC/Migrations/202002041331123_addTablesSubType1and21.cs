namespace TreeViewMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addTablesSubType1and21 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.VehicleSubType1",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                        VehicleSubTypeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.VehicleSubType", t => t.VehicleSubTypeId, cascadeDelete: true)
                .Index(t => t.VehicleSubTypeId);
            
            CreateTable(
                "dbo.VehicleSubType2",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                        VehicleSubType1Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.VehicleSubType1", t => t.VehicleSubType1Id, cascadeDelete: true)
                .Index(t => t.VehicleSubType1Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.VehicleSubType2", "VehicleSubType1Id", "dbo.VehicleSubType1");
            DropForeignKey("dbo.VehicleSubType1", "VehicleSubTypeId", "dbo.VehicleSubType");
            DropIndex("dbo.VehicleSubType2", new[] { "VehicleSubType1Id" });
            DropIndex("dbo.VehicleSubType1", new[] { "VehicleSubTypeId" });
            DropTable("dbo.VehicleSubType2");
            DropTable("dbo.VehicleSubType1");
        }
    }
}
