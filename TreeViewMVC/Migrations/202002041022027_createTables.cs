namespace TreeViewMVC.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class createTables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        Pid = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Categories", t => t.Pid)
                .Index(t => t.Pid);
            
            CreateTable(
                "dbo.Level",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Level1 = c.Int(nullable: false),
                        Level2 = c.Int(nullable: false),
                        Level3 = c.Int(nullable: false),
                        Level4 = c.Int(nullable: false),
                        Description = c.String(nullable: false, maxLength: 30),
                        Pid = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Level", t => t.Pid)
                .Index(t => new { t.Level1, t.Level2, t.Level3, t.Level4 }, unique: true, name: "IX_Level")
                .Index(t => t.Pid);
            
            CreateTable(
                "dbo.VehicleSubType",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                        VehicleTypeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Vehicle", t => t.VehicleTypeId, cascadeDelete: true)
                .Index(t => t.VehicleTypeId);
            
            CreateTable(
                "dbo.Vehicle",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.VehicleSubType", "VehicleTypeId", "dbo.Vehicle");
            DropForeignKey("dbo.Level", "Pid", "dbo.Level");
            DropForeignKey("dbo.Categories", "Pid", "dbo.Categories");
            DropIndex("dbo.VehicleSubType", new[] { "VehicleTypeId" });
            DropIndex("dbo.Level", new[] { "Pid" });
            DropIndex("dbo.Level", "IX_Level");
            DropIndex("dbo.Categories", new[] { "Pid" });
            DropTable("dbo.Vehicle");
            DropTable("dbo.VehicleSubType");
            DropTable("dbo.Level");
            DropTable("dbo.Categories");
        }
    }
}
