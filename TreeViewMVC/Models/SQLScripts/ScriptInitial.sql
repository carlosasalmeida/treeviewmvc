/****** Script for SelectTopNRows command from SSMS  ******/

  use TreeViewDB;

  delete from dbo.level where 1=1;

  SET IDENTITY_INSERT dbo.Level ON;   

  insert into dbo.Level(ID,Description,Level1,Level2,Level3,Level4,Pid) values (1,'Level 1'  ,1,0,0,0,null);
  insert into dbo.Level(ID,Description,Level1,Level2,Level3,Level4,Pid) values (2,'Level 1 1',1,1,0,0,1);
  insert into dbo.Level(ID,Description,Level1,Level2,Level3,Level4,Pid) values (3,'Level 2'  ,2,0,0,0,null);
  insert into dbo.Level(ID,Description,Level1,Level2,Level3,Level4,Pid) values (4,'Level 2 1',2,1,0,0,3);
  insert into dbo.Level(ID,Description,Level1,Level2,Level3,Level4,Pid) values (5,'Level 3'  ,3,0,0,0,NULL);
  insert into dbo.Level(ID,Description,Level1,Level2,Level3,Level4,Pid) values (6,'Level 3 1',3,1,0,0,5);

  insert into dbo.Level(ID,Description,Level1,Level2,Level3,Level4,Pid) values (7,'Level 4',4,0,0,0,null);
  insert into dbo.Level(ID,Description,Level1,Level2,Level3,Level4,Pid) values (8,'Level 3 1 1',3,1,1,0,6);
  insert into dbo.Level(ID,Description,Level1,Level2,Level3,Level4,Pid) values (9,'Level 3 1 1 1',3,1,1,1,8);

SET IDENTITY_INSERT dbo.Level OFF;   

  SELECT TOP (1000) [ID]
      ,[Level1]
      ,[Level2]
      ,[Level3]
      ,[Level4]
      ,[Description]
      ,[Pid]
  FROM [TreeViewDB].[dbo].[Level];

