use TreeViewDB;

--delete from dbo.VehicleType;
--delete from dbo.VehicleSubType;
--delete from dbo.VehicleSubType1;
--delete from dbo.VehicleSubType2;

--SET IDENTITY_INSERT dbo.VehicleType ON;   
--SET IDENTITY_INSERT dbo.VehicleSubType ON; 
--SET IDENTITY_INSERT dbo.VehicleSubType1 ON; 
SET IDENTITY_INSERT dbo.VehicleSubType2 ON; 
 
--INSERT INTO [VehicleType]    ([Id],[Name]) VALUES(1,'Cars');
--INSERT INTO [VehicleType] (Id,Name)  VALUES (2,'Bikes');
-------------------------- VehicleSubType ---------------------------------
--INSERT INTO [VehicleSubType] ([Id] ,[Name] ,[VehicleTypeId]) VALUES (1 , 'Alto' , 1);
--INSERT INTO [VehicleSubType] ([Id] ,[Name] ,[VehicleTypeId]) VALUES (2 , 'WagonR' ,1);
--INSERT INTO [VehicleSubType] ([Id] ,[Name] ,[VehicleTypeId]) VALUES (3 , 'Scorpio' , 1);
--INSERT INTO [VehicleSubType] ([Id] ,[Name] ,[VehicleTypeId]) VALUES (4 , 'Duster' ,1);
--INSERT INTO [VehicleSubType] ([Id] ,[Name] ,[VehicleTypeId]) VALUES (5 , 'Discover' ,2);
--INSERT INTO [VehicleSubType] ([Id] ,[Name] ,[VehicleTypeId]) VALUES (6 , 'Avenger' ,2);
--INSERT INTO [VehicleSubType] ([Id] ,[Name] ,[VehicleTypeId]) VALUES (7 , 'Unicorn' ,2);
--INSERT INTO [VehicleSubType] ([Id] ,[Name] ,[VehicleTypeId]) VALUES (8 , 'Karizma' ,2);

-------------------------- VehicleSubType1 ---------------------------------

--INSERT INTO [VehicleSubType1] ([Id] ,[Name] ,[VehicleSubTypeId]) VALUES (1 , 'Alto 1 ' , 1);
--INSERT INTO [VehicleSubType1] ([Id] ,[Name] ,[VehicleSubTypeId]) VALUES (2 , 'Alto 1 1' ,1);
--INSERT INTO [VehicleSubType1] ([Id] ,[Name] ,[VehicleSubTypeId]) VALUES (3 , 'Alto 1 1 1' , 1);
--INSERT INTO [VehicleSubType1] ([Id] ,[Name] ,[VehicleSubTypeId]) VALUES (4 , 'Alto 1 1 1 1' ,1);

--INSERT INTO [VehicleSubType1] ([Id] ,[Name] ,[VehicleSubTypeId]) VALUES (5 , 'WagonR 1 ' , 2);
--INSERT INTO [VehicleSubType1] ([Id] ,[Name] ,[VehicleSubTypeId]) VALUES (6 , 'WagonR 1 1' ,2);
--INSERT INTO [VehicleSubType1] ([Id] ,[Name] ,[VehicleSubTypeId]) VALUES (7 , 'WagonR 1 1 1' , 2);
--INSERT INTO [VehicleSubType1] ([Id] ,[Name] ,[VehicleSubTypeId]) VALUES (8 , 'WagonR 1 1 1 1' ,2);
-------------------------- VehicleSubType2 ---------------------------------

INSERT INTO [VehicleSubType2] ([Id] ,[Name] ,[VehicleSubType1Id]) VALUES (1 , 'WagonR 1 1 a' , 5);
INSERT INTO [VehicleSubType2] ([Id] ,[Name] ,[VehicleSubType1Id]) VALUES (2 , 'WagonR 1 1 a a' ,5);
INSERT INTO [VehicleSubType2] ([Id] ,[Name] ,[VehicleSubType1Id]) VALUES (3 , 'WagonR 1 1 a a a' , 5);
INSERT INTO [VehicleSubType2] ([Id] ,[Name] ,[VehicleSubType1Id]) VALUES (4 , 'WagonR 1 1 a a a a' ,5);

--SET IDENTITY_INSERT dbo.VehicleType OFF;
--SET IDENTITY_INSERT dbo.VehicleSubType OFF;
--SET IDENTITY_INSERT dbo.VehicleSubType1 OFF;
SET IDENTITY_INSERT dbo.VehicleSubType2 OFF;
