﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TreeViewMVC.Models
{
    [Table("Level")]
    public class Level
    {
        [Key]
        public int ID { get; set; }

        [Required]
        [Range(0, 999)]
        [Index("IX_Level", 1, IsUnique = true)]
        public int Level1 { get; set; }

        [Required]
        [Range(0, 999)]
        [Index("IX_Level", 2, IsUnique = true)]
        public int Level2 { get; set; }

        [Required]
        [Range(0, 999)]
        [Index("IX_Level", 3, IsUnique = true)]
        public int Level3 { get; set; }

        [Required]
        [Range(0, 999)]
        [Index("IX_Level", 4, IsUnique = true)]
        public int Level4 { get; set; }

        [Required]
        [MaxLength(30)]
        public string Description { get; set; }

        public int? Pid { get; set; }
        [ForeignKey("Pid")]
        public virtual Level Parent { get; set; }
        public virtual ICollection<Level> Childs { get; set; }




    }

}