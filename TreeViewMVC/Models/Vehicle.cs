﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TreeViewMVC.Models
{
    [Table("VehicleType")]
    public class VehicleType
    {
        [Key]
        public int ID { get; set; }

        [Required]
        [MaxLength(50)]
        public string Name { get; set; }

        public ICollection<VehicleSubType> VehicleSubTypes { get; set; }
    }

    [Table("VehicleSubType")]
    public class VehicleSubType
    {
        [Key]
        public int ID { get; set; }

        [Required]
        [MaxLength(50)]
        public string Name { get; set; }

        [ForeignKey("VehicleType")]
        public int VehicleTypeId { get; set; }

        public virtual VehicleType VehicleType { get; set; }
    }

    [Table("VehicleSubType1")]
    public class VehicleSubType1
    {
        [Key]
        public int ID { get; set; }

        [Required]
        [MaxLength(50)]
        public string Name { get; set; }

        [ForeignKey("VehicleSubType")]
        public int VehicleSubTypeId { get; set; }

        public virtual VehicleSubType VehicleSubType { get; set; }
    }
    [Table("VehicleSubType2")]
    public class VehicleSubType2
    {
        [Key]
        public int ID { get; set; }

        [Required]
        [MaxLength(50)]
        public string Name { get; set; }

        [ForeignKey("VehicleSubType1")]
        public int VehicleSubType1Id { get; set; }

        public virtual VehicleSubType1 VehicleSubType1 { get; set; }
    }

}