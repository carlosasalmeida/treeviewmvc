﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using TreeViewMVC.Models;

namespace TreeViewMVC
{
    public class context : DbContext
    {
        public context()
            : base("ConnectionString")
        {

        }

        public DbSet<Category> Categories { get; set; }
        public DbSet<Level> Levels { get; set; }

        public DbSet<VehicleType> VehicleTypes { get; set; }

        public DbSet<VehicleSubType> VehicleSubTypes { get; set; }

        public DbSet<VehicleSubType1> VehicleSubTypes1 { get; set; }
        public DbSet<VehicleSubType2> VehicleSubTypes2 { get; set; }

    }
}