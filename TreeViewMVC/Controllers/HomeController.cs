﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using TreeViewMVC.Models;
using TreeViewMVC.ViewsAux;

namespace TreeViewMVC.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        /// <summary>
        ///  It is do with database with two levels
        /// </summary>
        /// <returns>
        ///  After submit returns a json string with the checkboxes selected
        ///  When select a checkbox you select all fathers
        /// </returns>

        [HttpGet]
        public ActionResult TreeView()
        {
            var db = new context();
            return View(db.Categories.Where(x => !x.Pid.HasValue).ToList());
        }
        [HttpPost]
        public ActionResult TreeView(string selectedItems)
        {
            List<TreeViewNode> items = (new JavaScriptSerializer()).Deserialize<List<TreeViewNode>>(selectedItems);
            return RedirectToAction("Index");
        }
        /// <summary>
        ///  It is do with database with 4 levels
        /// </summary>
        /// <returns>
        ///  After submit returns a json string with the checkboxes selected
        ///  When select a checkbox you don't select the fathers
        /// </returns>
        [HttpGet]
        public ActionResult TreeViewLevel()
        {
            var db = new context();
            return View(db.Levels.Where(x => !x.Pid.HasValue).ToList());
        }
        [HttpPost]
        public ActionResult TreeViewLevel(string selectedItems)
        {
            List<TreeViewNode> items = (new JavaScriptSerializer()).Deserialize<List<TreeViewNode>>(selectedItems);
            return RedirectToAction("Index");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult TreeViewVehicle()
        {
            List<TreeViewNode> nodes = new List<TreeViewNode>();
            var db = new context();

            //Loop and add the Parent Nodes.
            foreach (VehicleType type in db.VehicleTypes)
            {
                nodes.Add(new TreeViewNode { id = type.ID.ToString(), parent = "#", text = type.Name });
            }

            //Loop and add the Child Nodes.
            foreach (VehicleSubType subType in db.VehicleSubTypes)
            {
                nodes.Add(new TreeViewNode { id = subType.VehicleTypeId.ToString() + "-" + subType.ID.ToString(), parent = subType.VehicleTypeId.ToString(), text = subType.Name });
            }

            //Serialize to JSON string.
            ViewBag.Json = (new JavaScriptSerializer()).Serialize(nodes);
            return View();
        }
        [HttpPost]
        public ActionResult TreeViewVehicle(string selectedItems)
        {
            List<TreeViewNode> items = (new JavaScriptSerializer()).Deserialize<List<TreeViewNode>>(selectedItems);
            return RedirectToAction("TreeViewVehicle");
        }


    }
}