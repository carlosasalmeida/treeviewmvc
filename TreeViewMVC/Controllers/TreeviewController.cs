﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TreeViewMVC.Trees;

namespace TreeViewMVC.Controllers
{
    public class TreeviewController : Controller
    {
        // GET: Treeview
        public JsonResult GetRoot()
        {
            List<JsTreeModel> items = GetTree();

            return new JsonResult { Data = items, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public JsonResult GetChildren(string id)
        {
            List<JsTreeModel> items = GetTree(id);

            return new JsonResult { Data = items, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        static List<JsTreeModel> GetTree()
        {
            var items = new List<JsTreeModel>();

            // set items in here

            return items;
        }

        static List<JsTreeModel> GetTree(string id)
        {
            var items = new List<JsTreeModel>();

            // set items in here

            return items;
        }

    }
}
